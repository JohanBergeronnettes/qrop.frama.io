# Comment modifier le site Internet de Qrop

## Introduction

Le site Internet de Qrop est un site statique, généré par l'outil [mkdocs](https://www.mkdocs.org/).

Le principe est le suivant : vous faites des modifications du site sur votre poste de travail et vous proposez ces modifications via Git via une Merge Request.

## Pré-requis

Il est nécessaire d'installer mkdocs pour pouvoir proposer des modifications du site Internet.

### Installation de mkdocs

mkdocs nécessite [Python](https://www.python.org/downloads/) et [pip](https://pip.readthedocs.io/en/stable/installing/) pour pouvoir être installé. 

Veuillez vous référer aux différentes documentations d'installation de ces deux outils, qui dépendent du système que vous utiliser (Linux, MacOS, Windows...).

Une fois Python et pip en place, il faut installer mkdocs, grâce aux commandes suivantes depuis un terminal :

```
$> pip install mkdocs
$> pip install mkdocs-material
```

### Téléchargement du code source du site

Il suffit de clôner le site sur votre poste de travail :

```
$> git clone git@framagit.org:qrop/qrop.frama.io.git
```

## Visualisation du site sur votre poste de travail

À partir du répertoire du site, lancer mkdocs :

```
$> mkdocs serve
```

Puis connectez-vous sur [http://127.0.0.1:8000](http://127.0.0.1:8000) pour prévisualiser le site.

Quand vous modifiez une page du site dans le code source, il suffit de recharger la page dans votre navigateur pour voir apparaître les modifications.

Quand vous êtes satisfait(e) de vos modifications, vous pouvez proposer une Merge Request.
