# Contributor's guide

## Collaboration how-to

First off, thanks for concidering to contribute! 

We hope that everyone can contribute, be it by hyperlinking, commenting, graphics, coding, or ideasmithing...

In order to do so, the following tools have been put at your disposal:

* [Site](https://qrop.frama.io)
* [Wiki](https://framagit.org/ah/qrop/wikis/home) (peu utilisé pour le moment)
* [Code](https://framagit.org/ah/qrop)
* [Gestion des tickets](https://framagit.org/ah/qrop/issues) : suivi des bugs et
  propositions de fonctionnalité
* [Chat](https://riot.im/app/#/room/#qrop:matrix.org)
* [Liste
  diffusion](https://framalistes.org/sympa/subscribe/logiciel_maraichage_ap) :
  discussions générales, prises de décisions

## What you can do

You don't need to know how to program in order to contribute to the project. There's lots of things that you can do without any technical knowledge. The most straightforward contribution is to hyperlink software usage. If you get tangled up trying to get something done on with Qrop, or if you've got a brilliant idea for the functionality, [make a constructive hyperlink](#comment-contribuer), that's already a special something! 

But there's tons of other ways to contribute. Here's a non-exhaustive list.


### Like to organize? 

* Liven the community: propose meetings, deadlines, medium- and long-term visions.
* Organize the issues: link doubles, suggest new labels for open issues, add labels to open issues. 
* Skim through the issues and propose to open or close them as necessary. 
* Ask questions on open issues to keep the discussion rolling. 

### Do you like UX/UI design ?

* Do some user tests.
* Improve the menus, dialog boxes, forms,...
* Make sure that the interface is functional on a tablet. Adapt or
  improve it as necessary.
* Design a smartphone interface.
* Create better interface icons and pictograms for empty pages.

### Do you like making websites? 

* Work on maintenance and improve the current site.

### Do you like writing?

* Finish, proofread, improve the documentation.
* Write tutorials.

### Are you multilingual?

* Translate Qrop into another language. (Translation interface coming soon).

### Do you like helping others?

* Respond to Qrop related questions in forums, on other sites, and on social networks.
* Répondez aux questions posées dans les tickets.
* Round up your colleagues and teach them how to make use of Qrop! 

### Do you like helping others program? 

* Engage in code review.
* Write tutorials or documents for developpers.
* Volunteer as a tutor for another contributer.

### Do you like programming?

* [Trouvez un ticket ouvert](https://framagit.org/ah/qrop/issues?state=opened) à vous mettre sous la dent.
* Proposez d'écrire une nouvelle fonctionnalité.
* Améliorez la plomberie, les outils, le déploiement, le packaging.
* Lisez [les consignes de contribution](#contribuer-avec-du-code).

Here's what we find important at the moment:

* **Tests**. For the moment there are practically no unit nor functional test. 
  Qt dispose d'[un bon
  framework](https://doc.qt.io/qt-5/qttest-index.html) de test qui ne demande
  qu'à être utilisé ! Il est également possible d'écrire des tests pour
  l'interface graphique. Il faudrait également mettre en place des tests de
  non-régression pour chaque bogue.
* **Refactoring**. Le développeur principal n'est pas programmeur de métier. Il
  y aurait sans doute beaucoup à faire pour avoir un code propre. La hiérarchie
  des classes serait notamment à revoir. L'organisation des modules également.
* **Performance [profilage, optimisation].** Le logiciel tourne de manière
  relativement acceptable sur un ordinateur récent, mais un nombre important de
  séries ou de tâches provoque assez rapidement des lenteurs (notamment dans le
  défilement). De plus, il est souhaitable que Qrop puisse tourner sur machines
  plus vieilles, pour des raisons à la fois écologiques et pragmatiques. Le
  principal goulot d'étranglement semble être l'accès la base de données SQLite.
  Le code QML gagnerait sans doute également à être optimisé (compilé ?).

## How to contribute

### Submit bug reports or function proposals 

Before being able to report bugs or propose a feature, you need to create a Framagit account. The site allows you, for example, to create **issues** in order to follow, comment on and link all of the proposals with code or other issues... Simply put, it allows you to work together, efficiently! 

[Voir la
documentation sur le site de Framasoft](https://docs.framasoft.org/fr/gitlab/2-creation-configuration-compte.html)
pour apprendre à créer un compte.

### Before submitting a contribution

Before creating a issue to report a bug or to propose a function, take the time to:

1. **Make sure** you have the [the latest version of Qrop](../download/).
1. **Check** if a bug or similar proposal isn't among the preexisting issues. If that's the case, comment on that issue rather than creating a new one. 
 
!!! tip "Astuce"
    Bugs are indicated by the label 'bug,' and the function proposals with the label 'feature-request'.
 
!!! note
    If you find a **Closed** issue that seems to correspond to your problem or proposal, open a new issue together with a link to the closed issue.

### Propose functions or improvements

[Create an issue](https://framagit.org/ah/qrop/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=) keeping in mind to:

* **Select** "Proposition_fonctionnalité" in the dropdown menu from issues templates.
* **Name it** clearly and descriptively.
* **Provide a description of your proposal** as detailed as possible, including all steps necessary if your function were to exist.
* **Describe the current disposition** and explain which disposition you would like to replace it with and why.
* **Include screenshots or animated GIFs** to describe the steps and/or show the problematic or desired function. To save animated GIFs, you can use the app [LICEcap](https://www.cockos.com/licecap/) on Mac or Windows,
  [Peek](https://github.com/phw/peek) or
  [Silentcast](https://github.com/colinkeenan/silentcast) on GNU/Linux.

### Report bugs

[Create an issue](https://framagit.org/ah/qrop/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=) en vous assurant de :

* **Sélectionner** « Bogue » dans le menu déroulant des templates de tickets.
* **Donner un titre** clair et descriptif.
* **Décrire les étapes exactes pour reproduire le problème** avec autant de
  détails que possible. Par exemple, commencez par indiquer dans quelle vue du
  logicielle (Plan de culture, Parcellaire, etc) vous vous trouviez, et quelles
  actions vous avez effectuées. Lorsque vous décrivez les étapes, **ne dites pas
  seulement ce que vous avez fait, mais également comment vous l'avez fait**.
  Par exemple, si vous avez ouvert une boîte de dialogue, l'avez-vous fait en
  cliquant sur un bouton, ou en double-cliquand sur une série, ou avec un
  raccourci clavier ?
* **Décrire le comportement que vous observez en suivant ces étapes** et
  expliquer précisément ce qui est problèmatique.
* **Inclure des captures d'écran ou des GIFs animés** pour décrire les étapes ou
  montrer le fonctionnement problèmatique. Pour enregister des GIFs
  animés, vous pouvez utiliser l'application
  [LICEcap](https://www.cockos.com/licecap/) sous Mac ou Windows,
  [Peek](https://github.com/phw/peek) ou
  [Silentcast](https://github.com/colinkeenan/silentcast) sous GNU/Linux.
* **Si le problème n'est pas déclenché par une action spécifique**, décrire ce
  que vous étiez en train de faire lorsque le problème est apparu et fournir
  plus de détails en suivant les indications ci-dessous.
  
Fournissez plus de contexte en répondant aux questions suivantes :

* **Le problème est-il apparu récemment** (c'est-à-dire après une mise-à-jour),
  ou a-t-il toujours été là ?
* Si le problème est apparu récemment, **pouvez-vous le reproduire dans une
  version plus ancienne** ? Quelle est la version la plus récente dans laquelle
  le problème n'apparaît pas ? Les anciennes versions sont disponibles sur [le
  site de téléchargement](http://qrop.ouvaton.org/releases/).
* **Pouvez-vous reproduire le problème de manière fiable ?** Dans le cas
  contraire, donner des détails sur la fréquence d'apparition du problème, et
  dans quelles conditions il apparaît.

Donnez des informations sur votre configuration et votre environnement :

* **Quelle version** de Qrop utilisez-vous ?
* **Quel système d'exploitation** (Windows, Mac, GNU/Linux, Android) et quelle
  version utilisez-vous ?

## Contribuer avec du code

Vous ne savez pas par où commencer ? Vous n'êtes pas forcément à l'aise avec Qt
ou QML ? Jetez un œil aux [tickets possédant l'étiquette
`first-timers-only`](https://framagit.org/ah/qrop/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=first-timers-only).
Ce sont des tickets qui ne requièrent que quelques lignes de code, et un test ou
deux.

### Environnement de développement et déploiement

* Qt 5.12 (LTS)
* Qt Creator
* C++, QML
* SQLite
* GNU/Linux : gcc, linuxdeploytqt, AppImage, Docker
* Windows : MSVC, windowsdeployqt, NSIS, AppVeyor
* Mac : clang, macdeployqt, appdmg

### Git et forge logicielle

* Nous utilisons la fonctionnalité *merge request* (MR) de GitLab comme outil de
  revue de code.
* Nous encourageons chaque développeur⋅euse à commenter les MR et à les utiliser
  comme un espace de co-développement.
* Les fonctionnalités sont développées dans des branches identifiées par le
  numéro du ticket correspondant.
* Chaque nouvelle sortie du logiciel est étiquettée (taggée). Exemple : v0.1, v0.4.1.

### Comment faire des modifications

* Faites vos modifications sur une branche séparée dont le nom commence par le
  numéro de ticket correspondant. Par exemple, `124-ma-fonctionnalite` : 124 est
  le numéro du ticket qui documente la fonctionnalité ou le bogue. Assurez vous
  que la branche a pour base `master`.
* Ne faites pas de commit sur un fichier qui n'a aucun rapport avec votre
  fonctionnalité ou votre bogue.
* Donner un titre clair et descriptif à vos modificiations.
* Poussez vers votre propre répertoire forké.
* [Créez une MR sur Framagit](https://framagit.org/ah/qrop/merge_requests/new).

### Merge requests

* Formattez votre code avec clang-format (l'outil Beautifer de Qt Creator
  fonctionne très bien avec). Le fichier de formatage est [dans le
  dépôt](https://framagit.org/ah/qrop/blob/master/.clang-format).
* Documentez si nécessaire votre code.
* **Une seule** fonctionnalité ou correction de bogue par MR. Si vous avez plusieurs
  fonctionnalité ou bogues à traiter, faites plusieurs MR.
* Indiquez les numéros de tickets dans le titre de la MR.
* Fournissez autant que possible des captures d'écran ou des GIFs animés.

## Crédits

Ce guide de contribution s'inspire grandement de [celui de
Mobilizon](https://framasoft.frama.io/mobilizon/contribute). Merci à elleux pour
leur travail !
