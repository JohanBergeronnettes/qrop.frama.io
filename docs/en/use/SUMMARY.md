# Summary

* [Installation](installing.md)
* [Overview](overview.md)
* [Planting map](planting-map.md)
* [Task calendar](task-calendar.md)
  * [Task templates](task-templates.md)
* [Location view](location-view.md)
* [Harvests](harvests.md)
* [Seed transplants](seeds_transplants.md)
* [Settings](settings.md)
* [Contribute](contrib.md)
