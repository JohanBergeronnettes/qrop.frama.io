# 🐞 Report a bug

> Before being able to report bugs, you need to create a Framagit account. 


## Step 1: Look for similar bugs

Before creating a bug report, take a minute to check that similar bug isn't already on the issues list.

This avoids creating doubles. Don't worry. Should this happen, it will be taken care of. *If you find a similar bug report, comment on that one rather than creating a new one. 

<a href="https://framagit.org/ah/qrop/issues?label_name%5B%5D=bug">
<i class="fab fa-gitlab"></i>
Skim through the bug list
</a>

## Step 2: Write your bug report

Select « Bug » in the scroll-down menu of issue templates and fill in the name and nature of your bug report.

Try to write your report as precise and specific as possible: other contributers reading your report may not know what seems obvious to you.

For sure, writing a good bug report isn't a piece of cake. Do your best, give as much info as possible, but don't fret: if necessary, other contributers will ask for further details. 

<a href="https://framagit.org/ah/qrop/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=" class="btn btn-outline">
<i class="fab fa-gitlab"></i>
Writing a bug report
</a>


## Step 3: Proofread and submit your report

Give your report a final look over!

Then click on « Submit issue ». A contributor will have a look at it shortly!

# ✨ Propose a funtionality

> Before being able to report bugs, you need to create a Framagit account.

## Step 1: Look for similar propositions

It's possible that the functionality you're looking for has already been proposed by someone else. Take a minute to sift through the issues list to be sure no similar proposition has already been made. Proposed issues have the tag *feature-request*.

If you find a similar proposition to yours, it's more productive to join in on the discussion than to create a new issue. That way you avoid making doubles. But don't worry, if that comes up, someone will deal with it.

<a href="https://framagit.org/ah/qrop/issues?label_name%5B%5D=feature-request" class="btn btn-outline">
<i class="fab fa-gitlab"></i>
Sift through funtionality propositions
</a>

## Step 2: Write your funtionality proposition

Select « Proposition_fonctionnalité » in the scroll-down menu of the issues' templates then fill in the title and content of your bug report.

<!-- Essayez d'écrire un rapport aussi précis et spécifique que possible : les autres
contributrices et contributeurs qui liront votre rapport peuvent ne pas savoir
des choses qui vous paraissent évidentes.

C'est sûr, écrire un bon rapport de bogue n'est pas une mince affaire. Faites de
votre mieux, donnez le plus d'informations possible, mais ne vous en faites pas :
si nécessaire, d'autres contributrices ou contributeurs vous demanderont des
détails.
 -->
<a href="https://framagit.org/ah/qrop/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=" class="btn btn-outline">
<i class="fab fa-gitlab"></i>
Write a functionality proposition
</a>

## Step 3: Proofread then submit your proposition

Reread your report one last time!

Then click on the button « Submit ticket ». A contributor will have a look at it soon thereafter!
