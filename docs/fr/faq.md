# Foire aux questions

## Contributions

### Comment contribuer ?

[Voir la page dédiée.](../contrib)

### Je ne sais pas programmer, puis-je quand même contribuer ?

Oui, il y a milles façon de contribuer sans toucher à une seule ligne de code ! 
[Voir la page dédiée aux contributions](../contrib) pour en savoir plus.

### Comment soutenir financièrement le développement ?

André, le développeur principal, est avant tout maraîcher en installation. Il a
passé une bonne partie de l'année 2019 à développer Qrop à titre gracieux. Pour
l'aider à développer et maintenir Qrop, vous pouvez lui faire un don sur
[Liberapay](https://liberapay.com/ah), la plateforme de dons récurrents libre et
éthique !

## Formations

### Est-ce que des formations au logiciel sont prévues ?

Peut-être ! Les formations au logiciel sont organisées par l'Atelier paysan, en
général en automne/hiver. [Voir directement sur le site de l'Atelier
paysan](https://www.latelierpaysan.org/Formations) si des formations sont
prévues.

### Je n'ai pas trouvé de formations, puis-je en proposer une ?

Oui, si vous pensez pouvoir rassembler une dizaine de personnes. Il faut alors
contacter [la responsable
formation](mailto:a.sombardier@latelierpaysan.org?subject=[Qrop] Organisation
 d'une formation) de l'Atelier
paysan.


