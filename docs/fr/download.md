# Télécharger Qrop

## Version stable (0.4.5 « La Rauze »)

### Windows

<div>
<a href="https://qrop.ouvaton.org/releases/v0.4.5/Qrop-0.4.5-x64.exe" title="AppImage 64 bits" class="md-button">
Windows 64 bits
</a>
<a href="https://qrop.ouvaton.org/releases/v0.4.5/Qrop-0.4.5-x86.exe" title="AppImage 64 bits" class="md-button">
Windows 32 bits
</a>
</div>

Version minimale requise : Windows 7.

### macOS

<a href="https://qrop.ouvaton.org/releases/v0.4.5/Qrop-0.4.5.dmg" title="AppImage 64 bits" class="md-button">
DMG macOS 10.13
</a>

Nous [travaillons](https://framagit.org/ah/qrop/-/issues/173) à faire fonctionner Qrop sous macOS 10.12. Les versions
inférieures ne pourront malheureusement pas être prises en charge.


### GNU/Linux

<a href="https://qrop.ouvaton.org/releases/v0.4.5/Qrop-0.4.5-x86_64.AppImage" title="AppImage 64 bits" class="md-button">
AppImage 64 bits
</a>

Nous n'avons pour le moment pas d'AppImage 32 bits, ni de paquets pour les distributions les plus courantes.


## Version de développement

!!! warning "Avertissement" 

    Ces exécutables sont générés à chaque modification
    de la branche principale (`master`) du code source et sont 
    potentiellement instables ! À utiliser en connaissance de cause...

* [GNU/Linux (AppImage 64 bits)](http://qrop.ouvaton.org/snapshots/Qrop-nightly-x86_64.AppImage)
* [Mac](http://qrop.ouvaton.org/snapshots/Qrop-nightly.dmg)
* [Windows 64 bits](http://qrop.ouvaton.org/snapshots/Qrop-nightly-x64.exe)
* [Windows 32 bits](http://qrop.ouvaton.org/snapshots/Qrop-nightly-x86.exe)
