# Parcellaire et assolement

La vue *Parcellaire et assolement* permet la création et l'édition d'un parcellaire, ainsi que l'affectation des séries aux emplacements.

![Vue générale du parcellaire](img/crop-map-fr-01.png)

| Id  | Composant            | Description                                      |
| --: | :-------------       | :------------------------------------------------|
|   1 | Case à cocher d'affichage des séries | Permet d'afficher ou cacher le panneau des séries à affecter |
|   2 | Barre de recherche des séries | Permet de filtrer les séries de la saison en cours selon divers critères : espèce, variété, durée, etc. |
|  3  | Bouton fléché des saison | Permet de naviguer entre les années et les saisons. Les flèches extérieures permettent de changer de d'année, celles intérieures de saison. |
|   4 | Panneau des emplacements |  |
|   5 | Panneau des séries à placer | |
|   6 | Bouton d'édition du parcellaire | Permet l'édition du parcellaire : ajout, duplication et suppression d'emplacements. |
|   7 | Icône de conflit | S'affiche lorsque une séries affichée sur l'emplacement ne respecte pas l'intervalle de rotation de sa famille. Un survol de cet icône permet d'afficher la liste des conflits. |
|   8 | Icône de l'historique  | Afficher l'historique des séries de l'emplacement. |

## Création, édition du parcellaire

Lors de la première utilisation, il faut définir son parcellaire. Qrop permet de définir librement son parcellaire, quelque soit sa hiérarchie. Ainsi, on peut se contenter d'avoir des jardins composés de planche, ou à l'inverse avec des blocs composés de jardins, eux même composés de planches divisées en 6 bandes !

Pour illustrer notre propos, nous allons prendre le premier exemple, et définir un parcellaire composé de 6 jardins (numérotés de A à F) composés de 10 planches numérotées de 1 à 10.

![Vue générale du parcellaire](img/crop-map-fr-02.png)

En cliquant sur le bouton « Ajouter des emplacements » [1], la fenêtre suivante apparaît :

![Vue générale du parcellaire](img/crop-map-fr-03.png)

Cette fenêtre permet de définir les paramètres suivants :

| Id | Description |
| :-- | :------------- |
|   1 | Nom de l'emplacement. Il peut être de trois type : (1) numérique (1, 3, 37...); (2) une ou deux lettres ou (3) ou libre. |
|   2 | Longueur de l'emplacement.      |
|   3 | Largeur de l'emplacement.       |
|   4 | Nombre d'emplacements de ce type à créer.      |

Lors de la création de plusieurs emplacement, leurs noms sont générés automatiquement en fonction du nom entré :

 1. numérique : la suite est générée à partir du premier chiffre entre, si l'on entre 3 comme nom et 6 comme nombre, les noms générés seront 3, 4, 5, 6, 7, 8 ;
 2. une ou deux lettres : la suite est générée à partir de la dernière lettre : si l'on entre « SA » comme nom et 6 comme nombre, les noms générés seront SA, SB, SC, SD, SE, SF ;
 3. libre : un chiffre incrémenté est ajouté au nom.

 L'exemple de la fenêtre précédente donne le résultat suivant :

![Vue générale du parcellaire](img/crop-map-fr-04.png)

En sélectionnant les emplacements que nous venons de créer (nos jardins), nous
allons pouvoir y ajouter des sous-emplacements (nos planches). En cliquant sur «
Ajouter des sous-emplacements », la fenêtre d'ajout apparaît à nouveau. Il
suffit alors d'entrer « 1 » comme nom et 10 comme nombre pour terminer la
définition de notre parcellaire !

![Vue générale du parcellaire](img/crop-map-fr-05.png)

Les emplacement de premier niveau apparaissent alors sur fond jaune, ce qui
indique qu'ils possèdent des sous-emplacements. Si nous avions voulu ajouter un
troisième niveau, il aurait fallu sélectionner tous les emplacements de niveau,
et y ajouter des sous emplacements.

Tout emplacement ayant des sous-emplacements aura un fond de couleur différente
selon son niveau. Les emplacements sans sous-emplacements ont un fond blanc.

Pour sortir du mode d'édition du parcellaire, il faut à nouveau cliquer sur le
bouton [6].

## Affectation des séries

L'affectation des séries se fait par glisser-déposer du panneau des séries vers
le panneau des emplacements. Lorsque l'on survole le panneau des emplacements
avec une série, deux barres apparaissent indiquant les dates de plantation et de
fin de récolte de la série. Si le curseur indique un symbole d'interdiction,
cela signifie qu'il n'y a plus de place disponible pour cet emplacement.

En déposant une série sur un emplacement contenant des sous-emplacements (par
exemple sur un jardin et non pas une planche), la série sera automatiquement
affectée à tous les sous-emplacements disponibles et nécessaires.

![Ajout à un emplacement parent](img/field-map-add-to-parent.gif)

Il est également possible de faire un glisser-déposer en maitenant la touche
<kbd>Ctrl</kbd> appuyée afin d'affecter la série à tous les emplacements
suivants celui où l'on dépose la série.

![Ajout aux emplacements suivants](img/field-map-add-to-siblings.gif)


## Raccourcis clavier

| Raccourci | Action                                  |
| --------- | --------------------------------------  |
| <kbd>Ctrl</kbd> + <kbd>F</kbd> | Activer la barre de recherche. |
| <kbd>Ctrl</kbd> + <kbd>&larr;</kbd> | Aller à la saison précédente. |
| <kbd>Ctrl</kbd> + <kbd>&rarr;</kbd> | Aller à la saison suivante. |
| <kbd>Ctrl</kbd> + <kbd>&darr;</kbd> | Aller à l'année précédente. |
| <kbd>Ctrl</kbd> + <kbd>&uarr;</kbd> | Aller à l'année suivante. |
