# 🐞 Écrire un rapport de bogue

> Avant de pouvoir rapporter des bogues, il est nécessaire de créer un compte
> Framagit.


## Étape 1 : recherchez des bogues similaire

Avant de créer un ticket pour rapporter un bogue, prenez une minute ou deux pour
voir si un bogue similaire ne figure pas dans la liste des tickets.

Cela évite de créer des doublons. Mais n'ayez crainte, si cela devait quand même
arriver, quelqu'un⋅e s'en occuperait. Si vous trouver un rapport de bogue
similaire, commentez celui-ci plutôt que d'en créer un nouveau.

<a href="https://framagit.org/ah/qrop/issues?label_name%5B%5D=bug">
<i class="fab fa-gitlab"></i>
Parcourir la liste des bogues
</a>

## Étape 2 : Écrivez votre rapport de bogue

Sélectionnez « Bogue » dans le menu déroulant des templates de tickets et
remplissez le titre et le contenu de votre rapport de bogue.

Essayez d'écrire un rapport aussi précis et spécifique que possible : les autres
contributrices et contributeurs qui liront votre rapport peuvent ne pas savoir
des choses qui vous paraissent évidentes.

C'est sûr, écrire un bon rapport de bogue n'est pas une mince affaire. Faites de
votre mieux, donnez le plus d'informations possible, mais ne vous en faites pas :
si nécessaire, d'autres contributrices ou contributeurs vous demanderont des
détails.

<a href="https://framagit.org/ah/qrop/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=" class="btn btn-outline">
<i class="fab fa-gitlab"></i>
Écrire un rapport de bogue
</a>

## Étape 3 : Relisez et soumettez votre rapport

Relisez votre rapport une dernière fois !

Cliquez ensuite sur le bouton « Submit ticket » (soumettre le ticket). Un⋅e
contributeur⋅ice y jettera bientôt un œil !

# ✨ Proposer une fonctionnalité

> Avant de pouvoir rapporter des bogues, il est nécessaire de créer un compte
> Framagit.

## Étape 1 : Recherchez des propositions similaires

Il est possible que la fonctionnalité que vous voulez ait déjà été demandée par
quelqu'un d'autre. Prenez une minute ou deux pour chercher dans la listes
tickets qu'aucune proposition similaire n'ait déjà été faite. Les tickets de
proposition ont une étiquette *feature-request*.

Si vous trouvez un proposition similaire à la vôtre, il sera plus productif de
rejoindre la discussion plutôt que de créer un nouveau ticket. Cela évite de
créer des doublons. Mais n'ayez crainte, si cela devait quand même arriver,
quelqu'un⋅e s'en occuperait.

<a href="https://framagit.org/ah/qrop/issues?label_name%5B%5D=feature-request" class="btn btn-outline">
<i class="fab fa-gitlab"></i>
Parcourir les propositions de fonctionnalité
</a>

## Étape 2 : écrivez votre proposition de fonctionnalité

Sélectionnez « Proposition_fonctionnalité » dans le menu déroulant des templates
de tickets et remplissez le titre et le contenu de votre rapport de bogue.

<!-- Essayez d'écrire un rapport aussi précis et spécifique que possible : les autres
contributrices et contributeurs qui liront votre rapport peuvent ne pas savoir
des choses qui vous paraissent évidentes.

C'est sûr, écrire un bon rapport de bogue n'est pas une mince affaire. Faites de
votre mieux, donnez le plus d'informations possible, mais ne vous en faites pas :
si nécessaire, d'autres contributrices ou contributeurs vous demanderont des
détails.
 -->
<a href="https://framagit.org/ah/qrop/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=" class="btn btn-outline">
<i class="fab fa-gitlab"></i>
Écrire une proposition de fonctionnalité
</a>

## Étape 3 : relisez et soumettez votre proposition

Relisez votre rapport une dernière fois !

Cliquez ensuite sur le bouton « Submit ticket » (soumettre le ticket). Un⋅e
contributeur⋅ice y jettera bientôt un œil !
