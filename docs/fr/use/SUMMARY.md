# Sommaire

* [Installation](installing.md)
* [Vue d'ensemble](overview.md)
* [Plan de culture](planting-map.md)
* [Calendrier des tâches](task-calendar.md)
  * [Itinéraires (groupe de tâches)](task-templates.md)
* [Parcellaire](location-view.md)
* [Récoltes](harvests.md)
* [Semences et plants](seeds_transplants.md)
* [Paramètres](settings.md)
* [Contribuer](contrib.md)
