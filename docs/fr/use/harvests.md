# Récoltes

La vue *Récoltes* permet de consigner les récoltes effectuées pour chaque série,
afin de connaître le rendement et le temps de travail par série.

![Vue générale du panneau des récoltes](img/harvest-fr-01.png)

En cliquant sur le bouton « Imprimante » en haut à droite, une fenêtre de
dialogue s'ouvre, permettant d'enregistrer la liste complète des récoltes au
format PDF.

## Ajout et édition de récoltes

En cliquant sur le bouton « Ajouter des récoltes » ou sur une récolte, une
fenêtre de dialogue s'ouvre :

![Fenêtre d'ajout/édition des récoltes](img/harvest-dialog-fr.png)

Lors de l'ajout d'une récolte, par défaut, seule la liste des séries en cours de
récolte s'affiche. En cliquant sur une série, la liste est automatiquement
restreintes aux série de la même espèce. Il est également possible d'afficher
toutes les séries du plan de culture en décochant la case « En cours ».

Il est possible de sélectionner plusieurs séries. Dans ce cas, autant de
récoltes que de séries sélectionnées seront créées, et la quantité récoltée,
ainsi que le temps de travail renseignés seront équitablement répartis entre
chaque récolte.
