# Paramètres

La fenêtres des paramètres permet de renseigner certains paramètres généraux [1]
et d'éditer les différentes listes de familles, espèces, variétés, tâches, *etc*
[2].

Pour éditer une liste, il suffit de cliquer sur le nom de la liste ou sur le
bouton chevron correspondant.

![Vue générales des paramètres](img/settings-fr-01.png)

## Paramètres généraux

Deux types d'affichage des dates sont disponibles :

| Type           | Description    |
| :------------- | :------------- |
| Semaine        | Seul le numéro de la semaine est affiché. Si la date est de l'année précédente ou suivante, un chevron gauche (<) ou droit (>) précède le numéro de semaine.       |
| Complète       | L'affichage est du type jour/mois avec éventuellement l'année si celle-ci est différente de l'année en cours. |

Il est également possible d'activer l'afficher du nom de fournisseur à côté de
la variété, ce qui permet de facilement travailler avec une même variété chez
différents fournisseurs.

## Édition des familles

![Édition des familles, espèces et variétés](img/settings-fr-02.png)

Le tableau suivante présente la liste des composants de la fenêtre d'édition des familles :

|  Id | Composant                | Description                                 |
| --: | :--------------          | :-------------------------------------------|
|   1 | Bouton d'ajout de famille  | Ouvre une fenêtre de dialogue d'ajout de famille. |
| 2 | Bouton d'affichage des espèces ou variétés | Permet d'afficher les espèces ou les variété de la famille ou de l'espèce. |
| 3 | Menu déroulant de sélection du fournisseur ou de la rotation | Dans le cas d'une famille, ce menu permet de sélectionner le nombre d'années avant le retour sur une même planche d'une espèce de cette famille. Dans le cas d'une espèce, il permet de sélectionner le fournisseur de semences pour cette espèce. |
| 4 | Bouton d'ajout de variété | Ouvre une fenêtre de dialogue d'ajout de variété |
| 5 | Bouton d'ajout d'espèce | Ouvre une fenêtre de dialogue d'ajout de d'espèce|
| 6 | Bouton de suppression | Apparaît lors d'une survol avec la souris d'une famille, espèce ou variété. Attention, toutes les tâches, espèces ou variétés concernées seront supprimées ! |

## Édition des tâches

La fenêtre d'édition des tâches fonctionne de façon similaire à la fenêtre d'édition des familles.

<!-- ![Édition des types tâche](img/settings-fr-05.png) -->

## Édition des mots-clefs

<!-- ![Édition des mots-clefs](img/settings-fr-03.png) -->

## Édition des fournisseurs de semences

<!-- ![Édition des fournisseurs de semences](img/settings-fr-04.png) -->

## Édition des unités

<!-- ![Édition des unités](img/settings-fr-06.png) -->
