# Vue d'ensemble de Qrop

La fenêtre de l'application est composée :

1. d'un panneau latéral ;
2. d'une vue.

![Vue d'ensemble de Qrop](img/overview-fr.png)

## Le panneau latéral

*Le panneau latéral* permet d'accéder aux différentes vues de l'application.
Il contient les éléments suivants :

1. Plan de culture
1. Calendrier des tâches
1. Parcellaire
1. Récoltes
1. Semences et plants
1. Sélection des bases de données (3)
1. Paramètres (4)
1. Boîte à propos (5)

### Sélection des bases de données

Le bouton « 1 » représente la base de données principales. Un clic simple permet
de la sélectionner. Un clic prolongé ouvre une boîte de dialogue permettant d'en
faire une copie de sauvegarde.

Le bouton « 2 » permet de créer, d'ouvrir ou d'exporter la base données
secondaire. Au démarrage de l'application, aucune base de données secondaire
n'est ouverte. Un clic simple ouvre un menu permettant de créer une nouvelle
base de données, d'en ouvrir une existante ou de l'exporter (si elle est
ouverte).

![Menu base de  données](img/database-menu-fr.png)

Une fois la base de données secondaire ouverte, un clic simple sur le bouton « 2
» permet de la sélectionner et clic prolongé d'ouvrir le menu.

## Raccourcis clavier globaux

| Raccourci | Action                                  |
| --------- | --------------------------------------  |
| <kbd>Ctrl</kbd> + <kbd>1</kbd> | Afficher le plan de culture. |
| <kbd>Ctrl</kbd> + <kbd>2</kbd> | Afficher le calendrier des tâches. |
| <kbd>Ctrl</kbd> + <kbd>3</kbd> | Afficher le parcellaire. |
| <kbd>Ctrl</kbd> + <kbd>4</kbd> | Afficher les récoltes. |
| <kbd>Ctrl</kbd> + <kbd>5</kbd> | Afficher les semences et plants. |
| <kbd>Ctrl</kbd> + <kbd>0</kbd>   | Afficher les paramètres de l'application. |
| <kbd>Ctrl</kbd> + <kbd>Tab</kbd> | Basculer entre les différentes vues (dans l'ordre). |
| <kbd>Ctrl</kbd> + <kbd>Q</kbd> | Quitter l'application. |
| <kbd>F11</kbd>                   | Activer ou désactiver le mode plein écran. |
