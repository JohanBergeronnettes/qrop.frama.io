# Itinéraires (groupe de tâches)

## Qu'est-ce qu'un itinéraire ?

Un itinéraire est un groupe de tâches typique. Cela permet de gagner du temps
lors de la planification : au lieu d'ajouter manuellement chaque tâche à chaque
série, il est possible de définir un groupe de tâches à appliquer à une ou
plusieurs séries. Les dates des tâches générées à partir d'un itinéraire sont
liées de façon dynamique aux séries.

La fenêtre des itinéraires est accessible depuis le calendrier des tâches en
cliquant sur le bouton « Itinéraires » :

![Vue générale des itinéraires](img/task-templates-fr-01.png)

Elle est composée de deux panneaux. À gauche se situe la liste des itinéraire, à
droite, la liste des tâches de l'itinéraire sélectionné.

## Création d'un nouvel itinéraire

En cliquant sur « Ajouter un itinéraire », une fenêtre apparaît, permettant de
saisir le nom du nouvel itinéraire.

## Duplication ou suppression d'un itinéraire

Pour dupliquer ou supprimer un itinéraire, il faut le survoler avec la souris :
les boutons « Dupliquer » et « Supprimer » apparaissent alors.

## Ajout d'une tâche à un itinéraire

La création d'une tâche d'itinéraire est similaire à celle d'une tâche normale.
Seule la saisie de la date change, car elle est relative à une des quatre dates
d'une série : semis en pépinière, semis/plantation en terre, début de récolte,
fin de récolte. La date est décrite par le champs « Planifier pour », les cases
« Avant/Après » et les cases des types de dates.

Dans l'exemple suivant, on ajoute ainsi un faux semis par occultation 42 jours
avant la date prévue de semis ou plantation en terre. De plus, cette tâche a une
durée au champs de 42 jours, et apparaîtra donc sur le parcellaire.

![Fenêtre d'ajout de tâche](img/task-templates-fr-02.png)

## Édition des tâches d'un itinéraire

L'édition des tâches d'un itinéraire est similaire à la création. Pour accéder à
la fenêtre d'édition d'une tâche, il faut cliquer sur la tâche ou sur le bouton
d'édition accessible en survolant la tâche.

Une fois la tâche éditée, une fenêtre de mise-à-jour apparaît :

![Mise-à-jour d'un itinéraire](img/task-templates-fr-03.png)

En répondant « Oui », toutes les tâches déjà créées (mais non effectuées) à
partir de cet itinéraire seront mises à jour. Dans le cas contraire, seules les
nouvelles tâches prendront en compte les modifications.


## Appliquer un itinéraire à une série

Pour appliquer un itinéraire à des séries, il faut se rendre sur le plan de culture.
En cliquant sur l'icône « Calendrier » en haut à droite, un panneau de tâches
s'affiche. En sélectionnant une seule série, l'ensemble des tâches relatives à
cette série apparaît. Dans la partie inférieure du panneau se trouve également
la liste des itinéraires définis.

![Panneau des tâches](img/task-templates-fr-crop-plan.png)

En cochant la case d'un itinéraire, les tâches de cet itinéraire sont créées et
liées de façon dynamique à chaque série sélectionnée :

![Itinéraire appliqué à une série](img/task-templates-fr-template-applied.png)

Lorsque plusieurs itinéraires sont appliqués à une même série, il est possible
de mettre en évidence les tâches d'un itinéraire en cliquant sur le nom de
l'itinéraire.

## Appliquer un itinéraire à plusieurs séries

Il est possible d'appliquer (ou d'enlever) des itinéraires à plusieurs séries
simultanément. Pour ce faire, il suffit de sélectionner plusieurs séries. Seules
les itinéraires communs à toutes les séries seront cochés. En cochant un
itinéraire, il sera appliqué à toutes les séries sélectionnées. En décochant, il
sera retiré de toutes les séries sélectionnées.

![Itinéraire appliqué à plusieurs tâches](img/task-templates-fr-multiple-plantings.png)

!!! tip "Astuce"
    En faisant une recherche « tomate », on affiche toutes les séries de
    tomate. On peut alors facilement sélectionner l'ensemble des séries de tomate,
    et leur appliquer l'itinéraire « Tomate ».
