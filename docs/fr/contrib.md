# Guide de contribution

## Comment nous collaborons

Tout d'abord, nous tenons à vous remercier d'envisager de contribuer !

Nous souhaitons que chacun⋅e puisse contribuer, que ce soit en faisant des
retours, des commentaires, ou avec du graphisme, du code, des idées géniales...

Pour ce faire, le projet dispose de plusieurs outils :

* [Site](https://qrop.frama.io)
* [Wiki](https://framagit.org/ah/qrop/wikis/home) (peu utilisé pour le moment)
* [Code](https://framagit.org/ah/qrop)
* [Gestion des tickets](https://framagit.org/ah/qrop/issues) : suivi des bugs et
  propositions de fonctionnalité
* [Chat](https://riot.im/app/#/room/#qrop:matrix.org)
* [Liste
  diffusion](https://framalistes.org/sympa/subscribe/logiciel_maraichage_ap) :
  discussions générales, prises de décisions

## Contributions possibles

Il n'est pas nécessaire de savoir programmer pour contribuer au projet. Il y a
beaucoup de choses que vous pouvez faire sans aucune connaissance technique. La
contribution la plus évidente, c'est de faire des retours sur l'utilisation du
logiciel. Si vous vous énervez sans parvenir à faire ce que vous voulez avec
Qrop, ou si vous avez une merveilleuse idée de fonctionnalité, [faites un retour
constructif](#comment-contribuer), ce sera déjà beaucoup !

Mais il y a beaucoup d'autres façon de contribuer. Voici une liste non
exhaustive.


### Vous aimez organiser ?

* Animez la communauté : proposez des rencontres, des échéances, une vision à
  moyen ou long terme.
* Organisez les [tickets](https://framagit.org/ah/qrop/-/issues){:target="_blank"} : liez les tickets en double, suggérez de nouvelles
  étiquettes, ajoutez des étiquettes aux tickets ouverts.
* Parcourez les tickets et proposez d'en rouvrir ou d'en fermer si nécessaire.
* Posez des questions sur des tickets ouverts afin de faire avancer la discussion.

### Vous aimez le design UX/UI  ?

* Faites des tests utilisateurs.
* Améliorez les menus, boîtes de dialogues, formulaires,...
* Vérifiez si l'interface est fonctionnelle sur tablette. Adaptez ou
  améliorez-la si nécessaire.
* Concevez une interface pour smartphone.
* Créez de meilleurs icônes pour l'interface, des pictogrammes pour les pages
  vides.

### Vous aimez faire des sites web ? 

* Faites la maintenance et améliorez le site actuel.

### Vous aimez écrire ?

* Complétez, relisez, améliorez la documentation.
* Écrivez des tutoriels.

### Vous êtes polyglotte ?

* Traduisez Qrop dans une autre langue. (Interface de traduction à venir).

### Vous aimez aider les autres ?

* Répondez aux questions concernant Qrop sur des forums, d'autres sites, les
  réseaux sociaux.
* Répondez aux questions posées dans les tickets.
* Apprenez à vos collègues à se servir de Qrop, essaimez !

### Vous aimez aider les autres à programmer ?

* Faites de la relecture de code.
* Écrivez des tutoriels ou de la documentation pour les développeur⋅euses.
* Proposez-vous comme tuteur⋅e pour une autre contributrice.

### Vous aimez programmer ?

* [Trouvez un ticket ouvert](https://framagit.org/ah/qrop/issues?state=opened){:target="_blank"} à vous mettre sous la dent.
* Proposez d'écrire une nouvelle fonctionnalité.
* Améliorez la plomberie, les outils, le déploiement, le packaging.
* Lisez [les consignes de contribution](#contribuer-avec-du-code).

Voici les thèmes qui nous paraissent importants en ce moment :

* **Tests**. Il n'y a actuellement quasiment pas de tests, ni unitaires, ni
  fonctionnels. Qt dispose d'[un bon
  framework](https://doc.qt.io/qt-5/qttest-index.html){:target="_blank"} de test qui ne demande
  qu'à être utilisé ! Il est également possible d'écrire des tests pour
  l'interface graphique. Il faudrait également mettre en place des tests de
  non-régression pour chaque bogue.
* **Refactoring**. Le développeur principal n'est pas programmeur de métier. Il
  y aurait sans doute beaucoup à faire pour avoir un code propre. La hiérarchie
  des classes serait notamment à revoir. L'organisation des modules également.
* **Performance [profilage, optimisation].** Le logiciel tourne de manière
  relativement acceptable sur un ordinateur récent, mais un nombre important de
  séries ou de tâches provoque assez rapidement des lenteurs (notamment dans le
  défilement). De plus, il est souhaitable que Qrop puisse tourner sur machines
  plus vieilles, pour des raisons à la fois écologiques et pragmatiques. Le
  principal goulot d'étranglement semble être l'accès la base de données SQLite.
  Le code QML gagnerait sans doute également à être optimisé (compilé ?).

## Comment contribuer

### Soumettre des rapports de bug ou des propositions de fonctionnalité

Avant de pouvoir rapporter des bogues ou proposer des fonctionnalités, il est
nécessaire de créer un compte sur la forge logicielle Framagit. Ce site permet
notamment de créer des **tickets** afin de suivre toutes les propositions, de
les commenter, de les lier avec du code ou d'autres tickets... Bref, cela permet
de travailler ensemble de façon efficace ! 

[Voir la
documentation sur le site de Framasoft](https://docs.framasoft.org/fr/gitlab/2-creation-configuration-compte.html)
pour apprendre à créer un compte.

### Avant de soumettre une contribution

Avant de créer un ticket pour rapporter un bogue ou proposer une fonctionnalité,
prenez le temps de :

1. **Vérifier** que vous avez bien [la dernière version de Qrop](../download/).
1. **Vérifier** si un bogue ou une proposition similaire ne figure pas dans la
 liste des tickets. Si tel est le cas, commenter celui-ci plutôt que d'en créer
 un nouveau. 
 
!!! tip "Astuce"
    Les bogues sont signalés avec l'étiquette `bug` et les propositions de
    fonctionnalité avec l'étiquette `feature-request`.
 
!!! note
    Si vous trouvez un ticket **Fermé** qui semble correspondre à votre problème
    ou proposition, ouvrez un nouveau ticket en incluant un lien vers le ticket fermé.

### Proposer des fonctionnalités ou des améliorations

[Créez un ticket](https://framagit.org/ah/qrop/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=) en vous assurant de :

* **Sélectionner** « Proposition_fonctionnalité » dans le menu déroulant des
  templates de tickets.
* **Donner un titre** clair et descriptif.
* **Fournir une description de votre proposition** aussi détaillée que possible,
  avec toutes les étapes qui selon vous seraient nécessaires si la fonctionnalité
  existait.
* **Décrire le comportement actuel** et expliquer quel comportement vous
  souhaiteriez à la place, et pourquoi.
* **Inclure des captures d'écran ou des GIFs animés** pour décrire les étapes ou
  montrer le fonctionnement problèmatique ou souhaité. Pour enregister des GIFs
  animés, vous pouvez utiliser l'application
  [LICEcap](https://www.cockos.com/licecap/) sous Mac ou Windows,
  [Peek](https://github.com/phw/peek) ou
  [Silentcast](https://github.com/colinkeenan/silentcast) sous GNU/Linux.

### Rapporter des bogues

[Créez un ticket](https://framagit.org/ah/qrop/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=) en vous assurant de :

* **Sélectionner** « Bogue » dans le menu déroulant des templates de tickets.
* **Donner un titre** clair et descriptif.
* **Décrire les étapes exactes pour reproduire le problème** avec autant de
  détails que possible. Par exemple, commencez par indiquer dans quelle vue du
  logicielle (Plan de culture, Parcellaire, etc) vous vous trouviez, et quelles
  actions vous avez effectuées. Lorsque vous décrivez les étapes, **ne dites pas
  seulement ce que vous avez fait, mais également comment vous l'avez fait**.
  Par exemple, si vous avez ouvert une boîte de dialogue, l'avez-vous fait en
  cliquant sur un bouton, ou en double-cliquand sur une série, ou avec un
  raccourci clavier ?
* **Décrire le comportement que vous observez en suivant ces étapes** et
  expliquer précisément ce qui est problèmatique.
* **Inclure des captures d'écran ou des GIFs animés** pour décrire les étapes ou
  montrer le fonctionnement problèmatique. Pour enregister des GIFs
  animés, vous pouvez utiliser l'application
  [LICEcap](https://www.cockos.com/licecap/) sous Mac ou Windows,
  [Peek](https://github.com/phw/peek) ou
  [Silentcast](https://github.com/colinkeenan/silentcast) sous GNU/Linux.
* **Si le problème n'est pas déclenché par une action spécifique**, décrire ce
  que vous étiez en train de faire lorsque le problème est apparu et fournir
  plus de détails en suivant les indications ci-dessous.
  
Fournissez plus de contexte en répondant aux questions suivantes :

* **Le problème est-il apparu récemment** (c'est-à-dire après une mise-à-jour),
  ou a-t-il toujours été là ?
* Si le problème est apparu récemment, **pouvez-vous le reproduire dans une
  version plus ancienne** ? Quelle est la version la plus récente dans laquelle
  le problème n'apparaît pas ? Les anciennes versions sont disponibles sur [le
  site de téléchargement](http://qrop.ouvaton.org/releases/).
* **Pouvez-vous reproduire le problème de manière fiable ?** Dans le cas
  contraire, donner des détails sur la fréquence d'apparition du problème, et
  dans quelles conditions il apparaît.

Donnez des informations sur votre configuration et votre environnement :

* **Quelle version** de Qrop utilisez-vous ?
* **Quel système d'exploitation** (Windows, Mac, GNU/Linux, Android) et quelle
  version utilisez-vous ?

## Contribuer avec du code

Vous ne savez pas par où commencer ? Vous n'êtes pas forcément à l'aise avec Qt
ou QML ? Jetez un œil aux [tickets possédant l'étiquette
`first-timers-only`](https://framagit.org/ah/qrop/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=first-timers-only).
Ce sont des tickets qui ne requièrent que quelques lignes de code, et un test ou
deux.

### Environnement de développement et déploiement

* Qt 5.12 (LTS)
* Qt Creator
* C++, QML
* SQLite
* GNU/Linux : gcc, linuxdeploytqt, AppImage, Docker
* Windows : MSVC, windowsdeployqt, NSIS, AppVeyor
* Mac : clang, macdeployqt, appdmg

### Git et forge logicielle

* Nous utilisons la fonctionnalité *merge request* (MR) de GitLab comme outil de
  revue de code.
* Nous encourageons chaque développeur⋅euse à commenter les MR et à les utiliser
  comme un espace de co-développement.
* Les fonctionnalités sont développées dans des branches identifiées par le
  numéro du ticket correspondant.
* Chaque nouvelle sortie du logiciel est étiquettée (taggée). Exemple : v0.1, v0.4.1.

### Comment faire des modifications

* Faites vos modifications sur une branche séparée dont le nom commence par le
  numéro de ticket correspondant. Par exemple, `124-ma-fonctionnalite` : 124 est
  le numéro du ticket qui documente la fonctionnalité ou le bogue. Assurez vous
  que la branche a pour base `master`.
* Ne faites pas de commit sur un fichier qui n'a aucun rapport avec votre
  fonctionnalité ou votre bogue.
* Donner un titre clair et descriptif à vos modificiations.
* Poussez vers votre propre répertoire forké.
* [Créez une MR sur Framagit](https://framagit.org/ah/qrop/merge_requests/new).

### Merge requests

* Formattez votre code avec clang-format (l'outil Beautifer de Qt Creator
  fonctionne très bien avec). Le fichier de formatage est [dans le
  dépôt](https://framagit.org/ah/qrop/blob/master/.clang-format).
* Documentez si nécessaire votre code.
* **Une seule** fonctionnalité ou correction de bogue par MR. Si vous avez plusieurs
  fonctionnalité ou bogues à traiter, faites plusieurs MR.
* Indiquez les numéros de tickets dans le titre de la MR.
* Fournissez autant que possible des captures d'écran ou des GIFs animés.

## Crédits

Ce guide de contribution s'inspire grandement de [celui de
Mobilizon](https://framasoft.frama.io/mobilizon/contribute). Merci à elleux pour
leur travail !
